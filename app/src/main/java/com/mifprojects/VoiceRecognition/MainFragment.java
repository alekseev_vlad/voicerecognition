package com.mifprojects.VoiceRecognition;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class MainFragment extends Fragment {

    private View mView;
    private BasePagerAdapter mViewPagerAdapter;
    private ViewPager mViewPager;
    private TabLayout mTabLayout;

    private OnlineRecognitionFragment mOnlineFragment = new OnlineRecognitionFragment();
    private OfflineRecognitionFragment mOfflineFragment = new OfflineRecognitionFragment();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (mView != null)
            return mView;

        mView = inflater.inflate(R.layout.fragment_main, container, false);

        mViewPager = (ViewPager) mView.findViewById(R.id.viewpager);
        mTabLayout = (TabLayout) mView.findViewById(R.id.tab_layout);

        mViewPagerAdapter = new BasePagerAdapter(getChildFragmentManager());
        mViewPagerAdapter.addFragment(mOnlineFragment, "Google Online");
        mViewPagerAdapter.addFragment(mOfflineFragment, "Offline");
        mViewPager.setAdapter(mViewPagerAdapter);
        mTabLayout.setupWithViewPager(mViewPager);

        return mView;
    }
}
