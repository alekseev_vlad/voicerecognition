package com.mifprojects.VoiceRecognition;

import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import es.claucookie.miniequalizerlibrary.EqualizerView;

public class OnlineRecognitionFragment extends Fragment implements View.OnClickListener {

    private View mView;

    private TextView mText;
    private SpeechRecognizer speechRecognizer;
    private static final String TAG = "MyStt3Activity";

    private ImageView btnSpeak;
    private EqualizerView mEqualizer1;
    private EqualizerView mEqualizer2;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView != null)
            return mView;

        mView = inflater.inflate(R.layout.fragment_online_recognition, container, false);

        initViews();

        btnSpeak.setOnClickListener(this);
        speechRecognizer = SpeechRecognizer.createSpeechRecognizer(getContext());
        speechRecognizer.setRecognitionListener(new listener());

        return mView;
    }

    private void initViews() {
        mEqualizer1 = (EqualizerView) mView.findViewById(R.id.equalizer_view1);
        mEqualizer2 = (EqualizerView) mView.findViewById(R.id.equalizer_view2);
        btnSpeak = (ImageView) mView.findViewById(R.id.btnSpeak);
        mText = (TextView) mView.findViewById(R.id.textView1);
    }

    private void hideEqualizer() {
        mEqualizer1.stopBars(); // When you want equalizer stops animating
        mEqualizer2.stopBars();
    }

    private void showEqualizer() {
        mEqualizer1.animateBars(); // Whenever you want to start the animation
        mEqualizer2.animateBars();
    }

    class listener implements RecognitionListener {
        public void onReadyForSpeech(Bundle params) {
            Log.d(TAG, "onReadyForSpeech");
        }

        public void onBeginningOfSpeech() {
            Log.d(TAG, "onBeginningOfSpeech");
        }

        public void onRmsChanged(float rmsdB) {
            Log.d(TAG, "onRmsChanged");
        }

        public void onBufferReceived(byte[] buffer) {
            Log.d(TAG, "onBufferReceived");
        }

        public void onEndOfSpeech() {
            Log.d(TAG, "onEndOfSpeech");
        }

        public void onError(int error) {
            Log.d(TAG, "error " + error);
            mText.setText("error " + error);
            hideEqualizer();
        }

        public void onResults(Bundle results) {
            String str = new String();
            Log.d(TAG, "onResults " + results);
            ArrayList data = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
            for (int i = 0; i < data.size(); i++) {
                Log.d(TAG, "result " + data.get(i));
                mText.setText(String.valueOf(data.get(i)));
                str += data.get(i);
            }
            hideEqualizer();
//            mText.setText("results: " + String.valueOf(data.size()));
        }

        public void onPartialResults(Bundle partialResults) {
            Log.d(TAG, "onPartialResults");
        }

        public void onEvent(int eventType, Bundle params) {
            Log.d(TAG, "onEvent " + eventType);
        }
    }

    public void onClick(View v) {
        if (v.getId() == R.id.btnSpeak) {
            showEqualizer();
            Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
            intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, "voice.recognition.test");

            intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 5);
            speechRecognizer.startListening(intent);
            Log.i("111111", "11111111");
        }
    }
}
