package com.mifprojects.VoiceRecognition;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

import edu.cmu.pocketsphinx.Assets;
import edu.cmu.pocketsphinx.Hypothesis;
import edu.cmu.pocketsphinx.RecognitionListener;
import edu.cmu.pocketsphinx.SpeechRecognizer;
import edu.cmu.pocketsphinx.SpeechRecognizerSetup;
import es.claucookie.miniequalizerlibrary.EqualizerView;

import static android.widget.Toast.makeText;


public class OfflineRecognitionFragment extends Fragment implements RecognitionListener {

    /* Named searches allow to quickly reconfigure the decoder */
    private static final String KWS_SEARCH = "wakeup";
    private static final String FORECAST_SEARCH = "forecast";
    private static final String DIGITS_SEARCH = "digits";
    private static final String PHONE_SEARCH = "phones";
    private static final String MENU_SEARCH = "menu";

    /* Keyword we are looking for to activate menu */
//    private static final String KEYPHRASE = "oh mighty computer";
    private static final String KEYPHRASE = "okaychkin";

    /* Used to handle permission request */
    private static final int PERMISSIONS_REQUEST_RECORD_AUDIO = 1;

    private TextView mTextResult;
    private ImageButton btnSpeak;
    private boolean isEqualizerShown = false;
    private TextView mCaptionText;
    private ProgressDialog progressDialog;
    private Vibrator mVibrate;

    private EqualizerView mEqualizer1;
    private EqualizerView mEqualizer2;

    private SpeechRecognizer mRecognizer;
    private View mView;

    private HashMap<String, Integer> captions;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView != null)
            return mView;

        mView = inflater.inflate(R.layout.fragment_offline_recognition, container, false);
        initViews();
        btnSpeak.setOnClickListener(onButtonMicrophoneClick);

        // Prepare the data for UI
        captions = new HashMap<String, Integer>();
        captions.put(KWS_SEARCH, R.string.kws_caption);
        captions.put(MENU_SEARCH, R.string.menu_caption);
        captions.put(DIGITS_SEARCH, R.string.digits_caption);
        captions.put(PHONE_SEARCH, R.string.phone_caption);
        captions.put(FORECAST_SEARCH, R.string.forecast_caption);

        progressDialog = getProgressDialog();
        progressDialog.show();

        int permissionCheck = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.RECORD_AUDIO);
        if (permissionCheck == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.RECORD_AUDIO}, PERMISSIONS_REQUEST_RECORD_AUDIO);

        }
        runRecognizerSetup();
        Log.d("www_vlad", "onCreate");
        return mView;
    }


    @Override
    public void onResume() {
        super.onResume();
        Log.d("www_vlad", "onResume");
        if (mRecognizer != null)
            mRecognizer.startListening(KWS_SEARCH);
    }

    private ProgressDialog getProgressDialog() {
        ProgressDialog pg = new ProgressDialog(getContext());
        pg.setTitle("Preparing");
        pg.setMessage("Preparation component for speech recognition.");
        pg.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pg.setIndeterminate(true);
        return pg;
    }

    private void runRecognizerSetup() {
        // Recognizer initialization is a time-consuming and it involves IO,
        // so we execute it in async task
        new AsyncTask<Void, Void, Exception>() {
            @Override
            protected Exception doInBackground(Void... params) {
                try {
                    Assets assets = new Assets(getActivity());
                    File assetDir = assets.syncAssets();
                    setupRecognizer(assetDir);
                } catch (IOException e) {
                    return e;
                }
                return null;
            }

            @Override
            protected void onPostExecute(Exception result) {
                if (result != null) {
                    mCaptionText.setText("Failed to init recognizer " + result);
                } else {
                    switchSearch(KWS_SEARCH);
                }
            }
        }.execute();
    }

    private void initViews() {
        mEqualizer1 = (EqualizerView) mView.findViewById(R.id.equalizer_view1);
        mEqualizer2 = (EqualizerView) mView.findViewById(R.id.equalizer_view2);
        mTextResult = (TextView) mView.findViewById(R.id.txtSpeechInput);
        mCaptionText = (TextView) mView.findViewById(R.id.caption_text);
        btnSpeak = (ImageButton) mView.findViewById(R.id.btnSpeak);
        mVibrate = (Vibrator) getContext().getSystemService(Context.VIBRATOR_SERVICE);
    }

    private void setupRecognizer(File assetsDir) throws IOException {
        // The recognizer can be configured to perform multiple searches
        // of different kind and switch between them

        mRecognizer = SpeechRecognizerSetup.defaultSetup()
                .setAcousticModel(new File(assetsDir, "en-us-ptm"))
                .setDictionary(new File(assetsDir, "cmudict-en-us.dict"))

                .setRawLogDir(assetsDir) // To disable logging of raw audio comment out this call (takes a lot of space on the device)
                .setKeywordThreshold(1e-7f)
                .getRecognizer();
        mRecognizer.addListener(this);

        /** In your application you might not need to add all those searches.
         * They are added here for demonstration. You can leave just one.
         */

        // Create keyword-activation search.
        mRecognizer.addKeyphraseSearch(KWS_SEARCH, KEYPHRASE);

        // Create grammar-based search for selection between demos
        File menuGrammar = new File(assetsDir, "menu.gram");
        mRecognizer.addGrammarSearch(MENU_SEARCH, menuGrammar);

        // Create grammar-based search for digit recognition
        File digitsGrammar = new File(assetsDir, "digits.gram");
//        File digitsGrammar = new File(assetsDir, "en_dict_gram.gram");
        mRecognizer.addGrammarSearch(DIGITS_SEARCH, digitsGrammar);

        // Create language model search
        File languageModel = new File(assetsDir, "weather.dmp");
        mRecognizer.addNgramSearch(FORECAST_SEARCH, languageModel);

        // Phonetic search
        File phoneticModel = new File(assetsDir, "en-phone.dmp");
        mRecognizer.addAllphoneSearch(PHONE_SEARCH, phoneticModel);

    }

    private void switchSearch(String searchName) {
        mRecognizer.stop();

        // If we are not spotting, start listening with timeout (10000 ms or 10 seconds).
        if (searchName.equals(KWS_SEARCH)) {
            progressDialog.hide();
            mRecognizer.startListening(searchName);
        } else {
//            showEqualizer();
            mRecognizer.startListening(searchName, 10000);
        }

        String caption = getResources().getString(captions.get(searchName));
        mCaptionText.setText(caption);

    }

    /**
     * In partial result we get quick updates about current hypothesis. In
     * keyword spotting mode we can react here, in other modes we need to wait
     * for final result in onResult.
     */
    @Override
    public void onPartialResult(Hypothesis hypothesis) {
        if (hypothesis == null)
            return;

        Log.d("www_vlad", "hypothesis " + hypothesis.getHypstr());
        String text = hypothesis.getHypstr();
        if (text.equals(KEYPHRASE)) {
            startToListeningIntent();
            switchSearch(DIGITS_SEARCH);
//            switchSearch(MENU_SEARCH);
        } else if (text.equals(DIGITS_SEARCH))
            switchSearch(DIGITS_SEARCH);
        else if (text.equals(PHONE_SEARCH))
            switchSearch(PHONE_SEARCH);
        else if (text.equals(FORECAST_SEARCH))
            switchSearch(FORECAST_SEARCH);
        else {
            mTextResult.setText(text);
        }
    }

    /**
     * This callback is called when we stop the recognizer.
     */
    @Override
    public void onResult(Hypothesis hypothesis) {
//        mTextResult.setText("");
        if (hypothesis != null) {
            String text = hypothesis.getHypstr();
            makeText(getContext(), text, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onBeginningOfSpeech() {
        showEqualizer();
    }

    /**
     * We stop recognizer here to get a final result
     */
    @Override
    public void onEndOfSpeech() {
        if (!mRecognizer.getSearchName().equals(KWS_SEARCH))
            switchSearch(KWS_SEARCH);

        hideEqualizer();
    }

    private void hideEqualizer() {
        mEqualizer1.stopBars(); // When you want equalizer stops animating
        mEqualizer2.stopBars();
    }

    private void showEqualizer() {
        mEqualizer1.animateBars(); // Whenever you want to start the animation
        mEqualizer2.animateBars();
    }

    @Override
    public void onError(Exception error) {
        mCaptionText.setText(error.getMessage());
    }

    @Override
    public void onTimeout() {
        switchSearch(KWS_SEARCH);
    }

    View.OnClickListener onButtonMicrophoneClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            onPartialResult(new Hypothesis(KEYPHRASE, 0, 0));
//            try {
//                generateFile();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
            showEqualizer();
        }

    };

//    private void generateFile() throws IOException {
//        InputStream ins = getResources().openRawResource(
//                getResources().getIdentifier("en_dict",
//                        "raw", getContext().getPackageName()));
//        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(ins));
//        try {
//            String line = bufferedReader.readLine();
//            while (line != null) {
//                appendLog(bufferedReader.readLine());
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//    public static void appendLog(String text) {
//        File logFile = new File("sdcard/en_dict_gram22.txt");
//        if (!logFile.exists()) {
//            try {
//                logFile.createNewFile();
//            } catch (IOException e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }
//        }
//        try {
//            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
//            String str = text + "   |";
//            buf.append(str);
//            buf.newLine();
//            buf.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//    }

    private void startToListeningIntent() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            callSound();
            mVibrate.vibrate(50);
        } else {
            mVibrate.vibrate(50);
        }
        mTextResult.setText("");
    }

    private void callSound() {
        MediaPlayer mediaPlayer = MediaPlayer.create(getActivity(), R.raw.notification);
        mediaPlayer.start();
//        Thread soundThread = new Thread() {
//            public void run() {
//                MediaPlayer mediaPlayer = MediaPlayer.create(getActivity(), R.raw.notification);
//                mediaPlayer.start();
//            }
//        };

//        soundThread.start();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == PERMISSIONS_REQUEST_RECORD_AUDIO) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                runRecognizerSetup();
            } else {
                getActivity().finish();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (mRecognizer != null) {
            mRecognizer.cancel();
            mRecognizer.shutdown();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d("www_vlad", "onStop");
        if (mRecognizer != null) {
            mRecognizer.stop();
        }
    }

}
